#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging 
from turtle import *

logging.basicConfig(filename='logger.log',level=logging.INFO)

logging.debug('debug message')
logging.info('info message')
logging.warn('warn message')
logging.error('error message')
logging.critical('critical message')

forward(100)
left(120)
forward(100)
left(120)
forward(100) 
