#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 数字

print('加法 + ',5+4)
print('减法 - ',4.3-2)
print('乘法 * ',3.1*7)
print('除法得到一个浮点数 / ',6/4)
print('除法得到一个整数 // ',6//4)
print('取余 % ',17%3)
print('乘方 ** ',2**5)
'''
    1、Python可以同时为多个变量赋值，如a, b = 1, 2。
    2、一个变量可以通过赋值指向不同类型的对象。
    3、数值的除法（/）总是返回一个浮点数，要获取整数使用//操作符。
    4、在混合计算时，Pyhton会把整型转换成为浮点数。
'''

# 字符串

# 运用*运算重复
print('str' + 'ing','my'*3)

'''
Python中的字符串有两种索引方式，第一种是从左往右，从0开始依次增加；第二种是从右往左，从-1开始依次减少。
注意，没有单独的字符类型，一个字符就是长度为1的字符串。 

对字符串进行切片，获取一段子串。用冒号分隔两个索引，形式为变量[头下标:尾下标]

 +---+---+---+---+---+
 | H | e | l | p | A |
 +---+---+---+---+---+
 0   1   2   3   4   5
-5  -4  -3  -2  -1

'''
word = 'Python'
print(word[0],word[5])
print(word[-1],word[-6])
print(word[1:3])

'''
    1、反斜杠可以用来转义，使用r可以让反斜杠不发生转义。
    2、字符串可以用+运算符连接在一起，用*运算符重复。
    3、Python中的字符串有两种索引方式，从左往右以0开始，从右往左以-1开始。
    4、Python中的字符串不能改变。
'''

# List(列表) 与String不同d是 list是可变的
mlist= ['him',25,100,'her']
print('list[0]:',mlist[0])
print('list[-1]:',mlist[-1])

print('List：',mlist,type(mlist),len(mlist))
'''
    1、List写在方括号之间，元素用逗号隔开。
    2、和字符串一样，list可以被索引和切片。
    3、List可以使用+操作符进行拼接。
    4、List中的元素是可以改变的。
'''

# Tuple(元组) 与list不同的是 元组的元组不可以修改，但是可以包含可变d对象，如list
mtuple = (1991,2014,'physics','math')
print('Tuple：',mtuple,type(mtuple),len(mtuple))

tup1 = () # 空元组
tup2 = (20,)#一个元素，需要在元素后加逗号

tup1, tup2 = (1, 2, 3), (4, 5, 6)
print('tuple + 操作符：',tup1+tup2) #(1, 2, 3, 4, 5, 6)
'''
string、list和tuple都属于sequence（序列）。

    1、与字符串一样，元组的元素不能修改。
    2、元组也可以被索引和切片，方法一样。
    3、注意构造包含0或1个元素的元组的特殊语法规则。
    4、元组也可以使用+操作符进行拼接。
'''

# Sets（集合） 是一个无序不重复元素d集
# 基本功能是进行成员关系测试和消除重复元素。
# 可以使用大括号 或者 set()函数创建set集合，注意：创建一个空集合必须用 set() 而不是 { }，因为{ }是用来创建一个空字典。 

#student = {'Tom','Jim','Mary','Tom','Jack','Rose'}
student = set(['Tom','Jim','Mary','Tom','Jack','Rose'])
print('Tom' in student)

a = set('abracadabra')
b = set('alacazam')
print(a,b)
print('差集：',a - b ) 
print('并集：',a | b)   
print('交集：',a & b)

# Dictionaries(字典)
'''
字典（dictionary）是Python中另一个非常有用的内置数据类型。
字典是一种映射类型（mapping type），它是一个无序的键 : 值对集合。
关键字必须使用不可变类型，也就是说list和包含可变类型的tuple不能做关键字。
在同一个字典中，关键字还必须互不相同。 
'''
dic = {}  # 创建空字典
tel = {'Jack':1557, 'Tom':1320, 'Rose':1886}
print(tel)
#{'Tom': 1320, 'Jack': 1557, 'Rose': 1886}
print(tel['Jack'])   # 主要的操作：通过key查询
del tel['Rose']  # 删除一个键值对
tel['Mary'] = 4127  # 添加一个键值对
print(tel)
#{'Tom': 1320, 'Jack': 1557, 'Mary': 4127}
list(tel.keys())  # 返回所有key组成的list
#['Tom', 'Jack', 'Mary']
print(sorted(tel.keys())) # 按key排序
#['Jack', 'Mary', 'Tom']
print('Tom' in tel)       # 成员测试
#True
print('Mary' not in tel)  # 成员测试
False

'''
注意：
    1、字典是一种映射类型，它的元素是键值对。
    2、字典的关键字必须为不可变类型，且不能重复。
    3、创建空字典使用{ }。
'''