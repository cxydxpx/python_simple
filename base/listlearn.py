#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 写在方括号之间、用逗号分隔开的数值列表。列表内的项目不必全是相同的类

mlist= ['him',25,100,'her']
print('list[0]:',mlist[0])
print('list[-1]:',mlist[-1])
print('list[-3:]',mlist[-3:])
print('list[:]',mlist[:])
mlist2 = mlist + ['she','me',666,8]
print(mlist2)
mlist[1] = 52
print('修改mlist[1]为52：',mlist)
mlist.append('end')
print('list,append("end"):',mlist)
mlist[1:3] = [2,3,4]
print('mlist[1:3] = [2,3,4]:',mlist)

print('---------------------------------------------')

# 元组 
'''
Python 的元组与列表类似，不同之处在于元组的元素不能修改。
元组使用小括号，列表使用方括号。
元组创建很简单，只需要在括号中添加元素，并使用逗号隔开即可。
'''
tup = ('google','learn',66,'java',888)
# 访问元组
print('Tuple:',tup)
# 创建空元组
tupNull = ()
# 只包含一个元素时，在后面加逗号
tupOne = (666,)
# 修改元组（不允许修改，只能拼接为新元组）
tupNew = ('firefox','github')
tup2 = tup + tupNew
print('拼接新元组：',tup2)
# 删除元组
del tupNew
#print(tupNew)
tupNum = (1,2,3,4,5,6,7)
print('tuple len():',len(tupNum))
print('tuple max():',max(tupNum))
print('tuple min():',min(tupNum))
print('将list转为tuple:',tuple(mlist))


print('---------------------------------------------')
# 字典
'''
字典是另一种可变容器模型，且可存储任意类型对象。
字典的每个键值(key=>value)对用冒号(:)分割，每个对之间用逗号(,)分割，整个字典包括在花括号({})中
键必须是唯一的，但值则不必。
值可以取任何数据类型，但键必须是不可变的，如字符串，数字或元组。

字典键的特性
1）不允许同一个键出现两次。创建时如果同一个键被赋值两次，后一个值会被记住
2）键必须不可变，所以可以用数字，字符串或元组充当，而用列表就不行

1  len(dict)计算字典元素个数，即键的总数。
2  str(dict)输出字典以可打印的字符串表示
3  type(variable)返回输入的变量类型，如果变量是字典就返回字典类型。
'''
mdict = {'Alice': '2341', 'Beth': '9102', 'Cecil': '3258','Cecil':'6666'}
print('查询dict:',mdict)
mdict['Beth'] = 'python'
print('修改字典 Beth参数：',mdict)
del mdict['Beth']
print('删除字典 Beth参数',mdict)
print('字典len()',len(mdict))
print('字典str()',str(mdict))
print('字典type()',type(mdict))
#mdict.clear()
#print('清除字典：',mdict)

'''
1	radiansdict.clear()
    删除字典内所有元素
2	radiansdict.copy()
    返回一个字典的浅复制
3	radiansdict.fromkeys()
    创建一个新字典，以序列seq中元素做字典的键，val为字典所有键对应的初始值
4	radiansdict.get(key, default=None)
    返回指定键的值，如果值不在字典中返回default值
5	key in dict
    如果键在字典dict里返回true，否则返回false
6	radiansdict.items()
    以列表返回可遍历的(键, 值) 元组数组
7	radiansdict.keys()
    以列表返回一个字典所有的键
8	radiansdict.setdefault(key, default=None)
    和get()类似, 但如果键不存在于字典中，将会添加键并将值设为default
9	radiansdict.update(dict2)
    把字典dict2的键/值对更新到dict里
10	radiansdict.values()
    以列表返回字典中的所有值
'''
