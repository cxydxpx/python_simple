#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# if elif else 语句
'''
    1、每个条件后面要使用冒号（:），表示接下来是满足条件后要执行的语句块。
    2、使用缩进来划分语句块，相同缩进数的语句在一起组成一个语句块。
    3、在Python中没有switch – case语句。
'''

#age = int(input("Age of the dog: "))
age = 2
if age < 0:
    print("This can hardly be true!") 
elif age == 1:  
    print("about 14 human years") 
elif age == 2:  
    print("about 22 human years") 
elif age > 2:
    human = 22 + (age -2)*5
    print("Human years: ", human)


print('---------------------------------------------')

# while 语句
n = 100
sum = 0
counter = 1
while counter <= n:
    sum = sum + counter
    counter += 1  
#    print("Sum of 1 until %d: %d" % (counter,sum)) 


print('---------------------------------------------')
# for 循环
mList = [1,2,3,4,5,6,7]
for x in mList:
    print('for循环：',x)
    if x == 1:
        print('for循环 break')
        break
# 以使range以指定数字开始并指定不同的增量(甚至可以是负数;有时这也叫做'步长'): 
for i in range(0, 10, 3) :
    print(i)


a = ['Mary', 'had', 'a', 'little', 'lamb']
for i in range(len(a)):
    print(i, a[i])
 
print('range生成一个list list(range(5):',list(range(5)))

print('---------------------------------------------')

'''
break和continue语句及循环中的else子句

break语句可以跳出for和while的循环体。如果你从for或while循环中终止，任何对应的循环else块将不执行。

continue语句被用来告诉Python跳过当前循环块中的剩余语句，然后继续进行下一轮循环。

循环语句可以有else子句;它在穷尽列表(以for循环)或条件变为假(以while循环)循环终止时被执行,但循环被break终止时不执行.

pass语句

pass语句什么都不做。它只在语法上需要一条语句但程序不需要任何操作时使用.例如:

while True:
    pass  # 等待键盘中断 (Ctrl+C)

'''


print('---------------------------------------------')

# 迭代器
iterList = [1,2,3,4]
it = iter(iterList)
for t in it:
    print('迭代器：',t)