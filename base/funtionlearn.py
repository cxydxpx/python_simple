#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def addFun(x,y):
    return x + y
#print(addFun(5,6))

import math

# 默认参数必须指向不变参数
def quadratic(a,b,c=1):#默认参数
    if not isinstance(a,(int,float)):
        raise TypeError('bad operand type')
    return abs(a) + abs(b) + abs(c)

print('普通参数:',quadratic(1,3,5)) # 普通参数
print('默认参数:',quadratic(1,2)) # 默认参数


# 可变参数

def calc(*numbers):
    sum = 0
    for n in numbers:
        sum = sum + n * n
    return sum
print('可变参数：',calc(1,2,3))

# 关键字参数

def person(name,age,*,city,job):
    print('关键字参数：','name:',name,'age:',age,'other:',args)

bob = {city='man',job='hangzhou'}
per('jack',28,bob)