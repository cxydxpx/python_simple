#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 操作网路d
import requests,sys
# 操作html标签d
from bs4 import BeautifulSoup

class downloader(object):

    def __init__(self):
        self.server = 'http://www.biqukan.com/'
        self.target = 'https://www.biqukan.com/1_1094/'
        self.names = [] # 存放章节名
        self.urls = [] # 存放章节链接
        self.nums = 0 # 章节数

    '''
    函数说明 获取下载链接
    '''
    def get_download_url(self):
        req = requests.get(url = self.target)
        #print('request.get(url)类型:',type(req))
        html = req.text
        #print('req.text 类型:',type(html))
        div_bf = BeautifulSoup(html)
        #print('BeautifulSoup(req.text) 类型：',type(div_bf))
        div = div_bf.find_all('div',class_ = 'listmain')
        #print('find_all(div class)类型：',type(div))
        a_bf = BeautifulSoup(str(div[0]))
        #print('BeautifulSoup(str(div[0]))类型：',type(a_bf))
        a = a_bf.find_all('a')
        #print('a_bf.find_all(a)类型：',a)
        self.nums = len(a[15:]) # 剔除不必要的章节
        for each in a[15:]:
            self.names.append(each.string)
            self.urls.append(self.server + each.get('href'))

    '''
    函数说明：获取章节内容
    '''
    def get_content(self,target):
        req = requests.get(url=target)
        html = req.text
        bf = BeautifulSoup(html)
        texts = bf.find_all('div',class_ = 'showtxt')
        texts = texts[0].text.replace('\xa0'*8,'\n\n')
        return texts

    '''
    将爬取的内容写入文件
    '''
    def writer(slef,name,path,text):
        write_flag = True
        with open(path,'a',encoding='utf-8') as f:
            f.write(name + '\n')
            f.writelines(text)
            f.write('\n\n')

if __name__ == '__main__':
    dl = downloader()
    dl.get_download_url()
    print('开始下载')
    for i in range(dl.nums):
        dl.writer(dl.names[i],'一年之恒',dl.get_content(dl.urls[i]))
        sys.stdout.write(' 已下载：%.3f%%'% float(i/dl.nums)+'\r')
        sys.stdout.flush()
    print('下载完成')
'''
if __name__ == '__main__':
    base = 'http://www.biqukan.com/'
    target = 'https://www.biqukan.com/1_1094/'
    req = requests.get(url=target)
    html = req.text
    bf = BeautifulSoup(html,'lxml')
    div = bf.find_all('div',class_ = 'listmain')

    divbf2 = BeautifulSoup(str(div[0]))
    a = divbf2.find_all('a')
    for each in a:
        print(each.string,base + each.get('href'))
'''